# 易群1.0

#### 官网
[https://bzghost.com](https://bzghost.com) 

>打造一款跨平台即时通信软件

>个人也能玩得转的IM




#### 目录

- uni-app   手机版支持 h5 ios android
- vue-pc    电脑版支持 window linux mac 
- vue-admin 后台管理系统
- vue-home  作用未知


#### 感谢
- `https://gitee.com/lele-666/V-IM.git` 
- `https://gitee.com/xiaowang0482/wechat.git`

#### 联系
QQ群  1025293030
微信  wandoufilm

#### 功能
- 消息类型
- [x] 群红包 
- [x] 文件
- [x] 图片 
- [x] 语音 
- [x] 视频 
- [x] 表情 
- [x] 撤回 
- [x] 转发 
- [x] @功能
- [x] 禁言

#### 技术
1. webscoket
1. vue
1. uniapp 
1. uView
1. electron-vue
1. iVew

#### 地址

> 账号：admin

> 密码：123456



后台: [后台 地址](http://120.27.95.106/index.html) 

PC : [EXE 下载](http://39.100.14.171/yiqun2.exe)

APP: [APK 下载](http://39.100.14.171/yiqun2.apk)

MAC H5 Linux: 暂未发布 



#### pc演示

![pc1](doc/img/微信截图_20201114220441.png)
![pc2](doc/img/微信截图_20201114221956.png)
![pc3](doc/img/微信截图_20201114223334.png)

#### app演示
 
![app0](doc/img/微信截图_20201201201326.png)
![app11](doc/img/微信截图_20201201170845.png)
![app1](doc/img/微信截图_20201117184615.png)
![app2](doc/img/微信截图_20201117184810.png)
![app3](doc/img/微信截图_20201117184821.png)
![app4](doc/img/微信截图_20201117184838.png)
![app5](doc/img/微信截图_20201117184845.png)

#### 后台演示
![logo](doc/img/微信截图_20201201211920.png)
![群组](doc/img/微信截图_20201201211950.png)
![用户](doc/img/微信截图_20201201212023.png)


